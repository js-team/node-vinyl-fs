# Installation
> `npm install --save @types/vinyl-fs`

# Summary
This package contains type definitions for vinyl-fs (https://github.com/gulpjs/vinyl-fs).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/vinyl-fs.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 20:08:00 GMT
 * Dependencies: [@types/glob-stream](https://npmjs.com/package/@types/glob-stream), [@types/node](https://npmjs.com/package/@types/node), [@types/vinyl](https://npmjs.com/package/@types/vinyl)

# Credits
These definitions were written by [vvakame](https://github.com/vvakame), [remisery](https://github.com/remisery), [TeamworkGuy2](https://github.com/TeamworkGuy2), and [Manuel Thalmann](https://github.com/manuth).
